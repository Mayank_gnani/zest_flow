#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: call_status.py
# Description: utility class for call status endpoint
# Author: Avinash
# Date: 18-06-2021

import sys
sys.path.append('../')
import os
from datetime import datetime

from logger_conf.logger import get_logger, CustomAdapter

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

#from main.custom_connector import logger, datetime

def refine_call_status(call_status_dict):
    # dstPhone
    cTime = datetime.now()
    callTime = cTime.isoformat()
    try:
        if call_status_dict["dstPhone"] != None:
            dstPhone = call_status_dict["dstPhone"]

        else:
            dstPhone = ''
    except Exception as e:
        logger.error("Error in dstPhone taking: Exception: "+ str(e)) 
        dstPhone = ''  

    # flow_id
    try:
        if call_status_dict["flow_id"] != None:
            flow_id = call_status_dict["flow_id"]

        else:
            flow_id = ''

    except Exception as e:
        logger.error("Error in flow_id taking: Exception :"+ str(e)) 
        flow_id = ''  
    
    try:
        if call_status_dict["callEndTime"] != None and call_status_dict["callEndTime"] != '':
            callEndTime = call_status_dict["callEndTime"][:19]
            callEndTime = datetime.strptime(callEndTime,'%Y/%m/%d %H:%M:%S')

            # Have to check the in-consistency in the date splitting
            date_split = call_status_dict["callEndTime"].split(" ")[0].split("/")
            new_date = date_split[2]+"/"+date_split[1]+"/"+date_split[0]

        else:
            callEndTime = ''

    except Exception as e:
        logger.error("Error in callEndTime taking: Exception : "+ str(e)) 
        callEndTime = '' 
        new_date = ''      
    
    try:
        if call_status_dict["callConnectedTime"] != None and call_status_dict["callConnectedTime"] != '':
            callConnectedTime = call_status_dict["callConnectedTime"][:19]
            callConnectedTime = datetime.strptime(callConnectedTime,'%Y/%m/%d %H:%M:%S')
        else:
            callConnectedTime = ''
    except Exception as e:
        logger.error("Error in callConnectedTime taking: Exception: "+ str(e))
        callConnectedTime = ''

    # changes related to new keys
    
    upload_call_info = {'call_status':call_status_dict["callStatus"],'conversation_start_time' : callConnectedTime,\
        'conversation_end_time' : callEndTime,'conversation_id' : str(call_status_dict["customerCRTId"]),\
        'source_phone': call_status_dict["srcPhone"],'ringing_time': int(call_status_dict["ringingTime"]), \
        'setup_time': int(call_status_dict["setupTime"]),'call_type':call_status_dict['callType'],\
        'call_date':new_date, 'flow_id':flow_id,'destination_phone':dstPhone,
        }
    
    return upload_call_info
    