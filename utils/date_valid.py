# -*- coding: utf-8 -*-
# Author: Roy

import sys
sys.path.append('../')
import datetime
import requests
import json
import re
from pytz import timezone
from dateutil import relativedelta
from db.tracker_store_connection import Haptik
from logger_conf.logger import get_logger

haptik_date = Haptik()

logger = get_logger(__name__)  # name of the module


format = "%d/%m/%Y %H:%M:%p"
# Current time in UTC
now_utc = datetime.datetime.now(timezone('UTC'))
# Convert to Asia/Kolkata time zone
now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
current_time_str = now_asia.strftime(format)
current_time= datetime.datetime.strptime(current_time_str, format).time()
current_date= datetime.datetime.strptime(current_time_str, format).date()


regex_dict = {
    "next_month":{
        "english": "next month|upcoming month",
    },
    "next_week":{
        "english": "next week|upcoming week",
    }
}


def validate_date(follow_date):

    date_format = "%d/%m/%Y"

    # followup date
    follow_up_date = datetime.datetime.strptime(follow_date, date_format).date()

    # current time
    today6pm = datetime.datetime.strptime('18:00:PM', "%H:%M:%p").time()
    today10am = datetime.datetime.strptime('10:00:AM', "%H:%M:%p").time()
        
    
    valid_date = (follow_up_date - current_date).days

    # today
    if valid_date == 0:
        #between 10 to 6
        if today10am < current_time < today6pm:
            return "valid"
        else:
            return "not_possible_today"
    # tomorrow and beyond
    elif valid_date >=1:
        return "valid"
    # before days
    else:
        return "invalid"

def evaluate_regex(lang, given_date):

    return_date = None
    date_format = "%d/%m/%Y"
    
    next_month_expression = regex_dict["next_month"][lang]
    next_month_regex_pattern = re.compile(next_month_expression)
    next_month_list = next_month_regex_pattern.findall(given_date)
    next_week_expression = regex_dict["next_week"][lang]
    next_week_regex_pattern = re.compile(next_week_expression)
    next_week_list = next_week_regex_pattern.findall(given_date)
    
    if len(next_month_list) > 0:
        return_date = datetime.date.today().replace(day=1) + relativedelta.relativedelta(months=1)
    elif len(next_week_list) > 0:
        return_date = datetime.date.today() + relativedelta.relativedelta(days=7)

    if return_date is not None:
        return_date = return_date.strftime(date_format)
    else:
        return given_date

    return return_date



def haptik_date_validation(given_date, language):
    
    code = {
        'hindi' : 'hi',
        'tamil' : 'ta',
        'telugu': 'te',
        'marathi': 'mr',
        'gujarati': 'gu',
        'malayalam': 'ml',
        'kannada': 'ka',
        'bengali': 'bn',
        'english':'en'
    }
    logger.info("=========== Inside haptik_date_validation")
    lang = code[language]
    #given_date2 = date_handling(lang, given_date)
    given_date2 = given_date
    try:
        # date taking
        try:
            urldate = haptik_date.connect_haptik('date',given_date, lang)
            logger.info("url for date:" + str(urldate))
            payload = {}
            headers = {}

            responseDate = requests.request("GET", urldate, headers=headers, data=payload)
            date = json.loads(responseDate.text.encode('utf8'))
        except Exception as e:
            logger.error("Error in request for Haptik date "+ str(e))

        if date['data'] is None:
            if given_date2 == given_date:
                # check for next month and next week
                responseDate = evaluate_regex(lang,given_date)
            else:
                responseDate = given_date2
                
            if responseDate is None:
                return 'not_proper_date'
            else:
                logger.info("Response after regex: "+ str(responseDate)) 
                logger.info("=========== End of haptik_date_validation")
                return responseDate
                
        else:
            dd = str(date['data'][0]['entity_value']['value']['dd'])
            mm = str(date['data'][0]['entity_value']['value']['mm'])
            yy = str(date['data'][0]['entity_value']['value']['yy'])

            follow_date = dd + '/' + mm + '/' + yy 
            logger.info("follow_date: "+ follow_date)       
    
            return follow_date

    except Exception as e:
        logger.error("Error in date taking "+ str(e)) 
