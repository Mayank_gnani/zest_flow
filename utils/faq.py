#!/usr/bin/env python3
# -- coding: utf-8 --
# Author: Roy


import sys
sys.path.append('../')
from rasa.core.tracker_store import RedisTrackerStore 
from rasa_sdk import Tracker
from db.tracker_store_connection import load_config
from logger_conf.logger import get_logger , CustomAdapter
from raven import Client
from db.tracker_store_connection import load_config,RedisDB
import json
logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})
# loading endpoints
gen_config = load_config()
# sentry connection
logger.info(gen_config["raven"])
client = Client(gen_config["raven"]["url"])
# redis connection
redis_db_obj = RedisDB()

# bot responses templates json
with open("../configs/templates_text.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)


#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def faq_body(tracker: Tracker):
    """
    Used for adding short messages at the end of each faqs
    return: short version of the main bot response
    """
    
    try:
        sender_id,cust_info = redis_db_obj.get_details(tracker)
        
        # temporary templates for fallback messages
        #-------------------------------------------------------------------------------------
        templates_temp = {}
        for key,value in templates.items():

            templates_temp[key] = value  
        #--------------------------------------------------------------------------------------


    except Exception as e:
        logger.error("Exception!!! Failed to read data from redis database in FAQ" + str(e))
        client.captureException()
    
    try:
        bot_msg  = ""
        for event in reversed(tracker.events):
            if event.get("event") == "bot":
                for template_key,template_message in templates_temp.items():

                    if event.get("text") == template_message:
                        short_key = template_key + "_short"				#if there is fallback message is present
                        if short_key in templates_temp:
                            bot_msg = templates_temp[short_key]
                            break
                        else:
                            bot_msg = templates_temp[template_key]
                            break
                break


        logger.info("Exit FAQ function with response: "+str(bot_msg), sender_id=sender_id)
        return bot_msg

    except Exception as e:
        logger.error("Exception!!! Tracker events to get bot_msg in FAQ" + str(e), sender_id=sender_id)
        client.captureException()
        bot_msg  = ""
        return bot_msg
