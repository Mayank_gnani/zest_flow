import sys

from flask import config
sys.path.append('../')

import os
import yaml
from logger_conf.logger import get_logger, CustomAdapter

# name of the module for logging 
logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})


def load_config():
    conf = ""
    try:
        environment = os.environ["BOT_ENV"]
        logger.info("Bot Environment is :"+str(environment))
        if environment == "prod":
            endpoint_filename = "endpoints.yml"                
        else:
            endpoint_filename = "endpoints_dev.yml" 
        with open('../configs/'+endpoint_filename, 'r') as f:
            conf = yaml.safe_load(f) 
    
    except Exception as e:
        logger.exception("Error loading the configuration from the endpoints.yml "+str(e))
    return conf

def load_meta(meta_data = "meta.yml"):
    meta = ""
    try:
        meta_data = meta_data 
        with open('../configs/'+meta_data, 'r') as f:
            meta = yaml.safe_load(f) 
    
    except Exception as e:
        logger.exception(f"Error loading the meta data from the meta.yml file"+str(e))
        # Checking if meta file existing or not
        if meta_data not in  os.listdir('../configs/'):
            logger.info(f"Meta data file does not exist in the location ../configs/ please check")
    return meta   

config = load_config()
meta   = load_meta()

# print("****meta",meta)
# print("**config",config)
dynamic_variables   = meta["dynamic_variables"] 
BOTCODE             = meta["code"][0]
THRESHOLD           = meta["threshold"][0]



redis_tracker_store = config['redis_tracker_store']
mongo_database      = config['mongo_database']
haptik              = config['haptik']
server              = config['server']
raven               = config['raven']
