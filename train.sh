#!/bin/bash
# python -m spacy download en_core_web_md
rasa train --fixed-model-name oyo_v1 --config ./configs/config.yml --domain ./configs/domain.yml
mkdir -p models
cd ./models
tar xvf oyo_v1.tar.gz
