# English Server

## Useful Commands
* Creating Docker Container

> **Default:** sudo docker run -itd -p Expose_Port:Rasa_Port --name Docker_container_name -w /path/to/working/dir/ --restart unless-stopped rasa:1.10.16

```
sudo docker run -itd -p 4007:5002 --name tvse_act -w /nlp_server --restart unless-stopped rasa:1.10.16
```

* Cross validating nlu data 
```
rasa test nlu -u data/nlu.md --config config.yml --cross-validation
```
* Train your rasa model
```
rasa train --fixed-model-name tvs_hin --config ./configs/config.yml --domain ./configs/domain.yml
```

* docker-entrypoint script
```python
#! /bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

LOG=OLD_LOGS
CONVO_LOG=convo_logs
if [ ! -d "$LOG" ]; then
    mkdir $LOG
fi

if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi

pkill -f -9 python3 
export LANG=C.UTF-8
cd main/
rasa run actions &
python3 rasa_server.py 

```

## Copy model from local to VM
```
scp -i "key.pem" /path/to/model/model.tar.gz VM@IP:/VM/project-folder/models
```