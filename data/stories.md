## payment path
* affirm
  - action_EMI

* affirm
  - action_payment


## paid path
<!-- - initial_message  -->
* alreadypaid
  - action_thanks

## later path
<!-- - initial_message  -->

* deny
  - action_later

## fallback path
<!-- - initial_message  -->
* affirm
  - action_EMI

* alreadypaid
  - action_thanks

## covid path
<!-- - initial_message  -->
* affirm
  - action_EMI

* deny
  - action_covid
* affirm
  - action_sorry

## covid problem path 1
<!-- - initial_message  -->
* affirm
  - action_EMI

* deny
  - action_covid
* deny
  - action_paylater
* affirm
  -  action_payment


## covid problem path 2
<!-- - initial_message  -->
* affirm
  - action_EMI

* deny
  - action_covid
* deny
  - action_paylater
* deny
  - action_connect
