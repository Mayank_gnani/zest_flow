import sys
sys.path.append('../')
import asyncio
import os
import re
import inspect
import json
import logging
import pytz
import pickle
import time
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable
from db.tracker_store_connection import load_meta
from operator import itemgetter
from asyncio import Queue, CancelledError
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from sanic import response
import rasa.utils.endpoints
from rasa.core.channels.channel import UserMessage, QueueOutputChannel
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import CollectingOutputChannel
from rasa.core import utils
from main.actions.get_config import templates
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from threading import Thread
from raven import Client
from utils.call_status import refine_call_status
from pytz import timezone
import random
from datetime import datetime, timezone
from datetime import date, timedelta

from logger_conf.logger import get_logger, CustomAdapter

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

# time in IST  
# utc_dt = datetime.datetime.now(timezone.utc)
IST = pytz.timezone('Asia/Kolkata')



# loading endpoints
gen_config = load_config()
# redis connection
client = Client(gen_config["raven"]["url"])
BOTCODE = load_meta()["code"][0]
mongo_conn = MongoDB()
redis_db_obj = RedisDB()




def modify_response(sender_id,response_text,flow_id):
    response_text = response_text.replace(",","~")
    logger.info('Entering modify response function')
    if flow_id != None:
        logger.info(f"Flow id is proper flow id : {flow_id}")
        flow_id = flow_id
    else:
        logger.info("Flow id is None so considering flow id : tvse")
        flow_id = "prod-tvse"
    nlp_response = {
        "sender_id": sender_id,
        "flow_id": flow_id,
        "next_flow_id": flow_id,
        "language": "en",
        "nlp_resp": response_text,
        "next_exp_utter_category": "",
        "next_exp_utter_time": "",
        "signal": ""
        }
    try:
        logger.info(f"flow id : {flow_id}")
        next_flow_id=flow_id
        signal=''
        language='en'
        next_exp_utter_category = ""
        if "|" in response_text:
            res_list=response_text.split('|')
            for a in res_list:
                a=a.strip(' ')
                if a=='hin':
                    response_text = response_text.replace("|", "").replace(a, "")
                    next_flow_id=f'{flow_id}-hin'
                    language='hi'
                if a=='DTMF10':
                    signal = "DTMF10"
                    response_text = response_text.replace("|", "").replace(a, "")
        if "EOC" in response_text or "TTA" in response_text:
            response_text = response_text.replace("EOC", "").replace("TTA", "")
            signal = "EOC"
        if "DTMF10" in response_text:
            response_text = response_text.replace("DTMF10", "")
            signal = "DTMF10"
        nlp_response = {
            "sender_id": sender_id,
            "flow_id": f"{flow_id}",
            "next_flow_id": next_flow_id,
            "language": language,
            "nlp_resp": response_text,
            "next_exp_utter_category": next_exp_utter_category,
            "next_exp_utter_time": "",
            "signal": signal
            }
        logger.info(nlp_response)
    except Exception as e:
        logger.exception("Exception in modify response function"+str(e))
    return nlp_response

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class CustomWebhook(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @classmethod
    def name(cls) -> Text:
        return "rest"

    @staticmethod
    async def on_message_wrapper(
        on_new_message: Callable[[UserMessage], Awaitable[Any]],
        text: Text,
        queue: Queue,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # pytype: disable=bad-return-type

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("message", None)

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def _extract_mobile(self, req: Request) -> Text:
        return req.json.get("mobile",None )or self.name()

    def stream_response(
        self,
        on_new_message: Callable[[UserMessage], Awaitable[None]],
        text: Text,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid pytype error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(json.dumps(result) + "\n")
            await task

        return stream  # pytype: disable=bad-return-type

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})
        
        @custom_webhook.route("/livecheck", methods=["GET"])
        async def livecheck(request: Request) -> HTTPResponse:
            logger.info("==++=livecheck=++==")
            return response.json({"status": "ok"})

        @custom_webhook.route("/readycheck", methods=["GET"])
        async def readycheck(request: Request) -> HTTPResponse:
            logger.info("==++=readycheck=++==")
            return response.json({"status": "ok"})
        
        def get_response_time(start_time):
            end_time = time.time()
            response_time = end_time - start_time
            return response_time, end_time
        
        @custom_webhook.route("/initial_message", methods=["GET"])
        async def initial_message(request: Request) -> HTTPResponse:
            prev = date.today().replace(day=1) - timedelta(days=1)
            try:

                logger.info('Argument Recieved are: '+str(request.args))
                logger.info('Mobile Number recieved is: '+str(request.args['mobile'][0]))
                mobile = int(request.args['mobile'][0])
           
            except Exception as e:
                logger.exception("")

            message = templates["initial_message"]
            response_json = {
                "text":message,  # Initial Message to be played
                "keys":[]
            }
            return response.json(response_json,status=200)
        
        @custom_webhook.route("/webhook/call_data/", methods=["POST"])
        async def call_data(request):
            logger.info("=============Entered call status function============")
            try:

                call_status_main_dict = request.json
                if "call_status" in call_status_main_dict:
                    call_status_dict = call_status_main_dict["call_status"]
                    phone_number = int(call_status_dict["customerPhone"])
                    sender_id = int(call_status_dict["customerCRTId"])

                    logger.info("Updating the call status for : " + str(sender_id))

                    current_date_time = datetime.now(IST)
                    todays_date_time = current_date_time.strftime("%d/%m/%Y %H:%M:%S")
                    # todays_date = current_date_time.strftime("%d/%m/%Y")

                    # last triggered date
                    last_triggered_date = datetime.strptime(todays_date_time, '%d/%m/%Y %H:%M:%S')

                    logger.info("call status dict:  " + str(call_status_dict))
                    upload_call_info = refine_call_status(call_status_dict)

                    try:
                        # taking data from redis if present
                        redis_data = redis_db_obj.redis_get_dict_value(str(sender_id))
                        call_status = call_status_dict["callStatus"]

                        

                        environment = os.environ["BOT_ENV"]
                        logger.info("Environment is : {}".format(environment))

                        if environment == "prod":
                            q1 = {"phone_number": phone_number, "active_call": 1}  # Row filter condition
                        else:
                            q1 = {"phone_number": phone_number}  # Row filter condition
                        q2 = {"_id": 0}  # Column filter condition

                        if redis_data is None:
                            # There is no data present in redis. Since the GRPC has not hit the bot's custom connector not even once
                            # This case will handle NO ANSWER and BUSY states and also ANSWERED if call get disconnected at the welcome message
                            logger.info("Call is {} and not data found in redis".format(call_status))
                          
                            
                            input_col_data = mongo_conn.mongo_get_collection_data(col_type="input", q1=q1, q2=q2)
                            
                            if call_status == "ANSWERED":
                                input_col_data['answered_seq'] += 1
                            else:
                                input_col_data['no_answer_seq'] += 1

                            input_col_data['sequence_number'] += 1
                            input_col_data['active_call'] = 0
                            input_col_data['conversation_log'] = []
                            input_col_data['last_triggered_date'] = last_triggered_date

                            update_dict = {**upload_call_info, **input_col_data}

                            is_updated = mongo_conn.mongo_update(col_type="input", mode="update", record=update_dict,
                                                                 upd_cond=q1)
                            logger.info("Update status is {} for sender id : {} and for phone number : {}".format(
                                str(is_updated), str(sender_id), str(phone_number)))

                            # Inserted updated record to mongo output collection
                            is_inserted = mongo_conn.mongo_update(col_type="output", mode="insert", record=update_dict)

                            logger.info("Update status is {} for sender id : {} and for phone number : {}".format(
                                str(is_inserted), str(sender_id), str(phone_number)))

                        # This case will handle 'ANSWERED':
                        else:                            
                            logger.info("Call is {} and data is found in redis".format(call_status))

                            input_col_data = redis_data
                            
                            input_col_data["channel"] = "telephony"
                
                            if call_status == "ANSWERED":
                                input_col_data['answered_seq'] += 1

                            input_col_data['sequence_number'] += 1
                            input_col_data['active_call'] = 0
                            input_col_data['last_triggered_date'] = last_triggered_date

                            update_dict = {**upload_call_info, **input_col_data}

                            is_updated = mongo_conn.mongo_update(col_type="input", mode="update", record=update_dict,
                                                                 upd_cond=q1)
                            logger.info("Update status is {} for sender id : {} and for phone number : {}".format(
                                str(is_updated), str(sender_id), str(phone_number)))

                            
                            # Inserted updated record to mongo output collection
                            is_inserted = mongo_conn.mongo_update(col_type="output", mode="insert", record=update_dict)

                            logger.info("Update status is {} for sender id : {} and for phone number : {}".format(
                                str(is_inserted), str(sender_id), str(phone_number)))

                    except Exception as e:
                        logger.exception("Exception - when inserting data to mongo : " + str(e))

                else:
                    logger.error("Error - Call status dictionary not recieved!")
                    status_update = {"call_status": "Call Status Updation Failure"}
                    return response.json(status_update)
            except Exception as e:
                logger.exception("Exception - When updating the call Status : " + str(e))
                status_update = {"call_status": "Call Status Updation Failure"}
                return response.json(status_update)

            status_update = {"call_status": "Call Status Updation Successul"}
            return response.json(status_update)
        
        @custom_webhook.route("/initiate", methods=["POST"])
        async def initiate(request):
            try:
                user_details = request.json
                try:
                    flow_id = user_details.get("flowid") or user_details.get("flow_id")
                except Exception as e:
                    logger.exception("Exception!! When getting the flow id from json : " +str(e))
                    flow_id = "oyo_apmt"
                         
                
                logger.info("Recieved the call request for inserting to DB : "+str(user_details))
                existing_details  = {
                    "month" : "october",
                    "last_triggered_date" :"",
                    "active_call" : 1,
                    "answered_seq" : 0,
                    "sequence_number" : 0,
                    "no_answer_seq" : 0,
                    "full_name" : user_details['name'],
                    "phone_number" : int(user_details['phone']),
                    "phone_number2" :int(user_details['phone']),
                    "duplicate" : 1,
                    "gnani_test":"gnani_test",
                    "call_comp_flag" : "",
                    "call_status" : "",
                    "stage" : "initial",
                    "flow_id" : flow_id,
                    "language" : "english"
                
                }

                # adding default dynamic variables
                meta_data = [i.replace('<','').replace('>','') for i in load_meta()['dynamic_variables']]
                new_meta_data = {}
                for i in meta_data:
                    if existing_details.get(i) != None:
                        continue
                    else:
                        new_meta_data[i] = ""

                existing_details = {**existing_details,**new_meta_data}

                # updated_dict = {**existing_details, **user_details}
                # check if the collection already exists and if present delete previous one
                condition = {"phone_number": int(user_details['phone'])}
                file_count = mongo_conn.mongo_file_count(col_type="input",q1=condition) 
                logger.info("custom_connector: file count: "+str(file_count))
                insert = {
                        "flow_id":flow_id,
                        "active_call": 1,
                        "language":"english",
                    
                    }
                logger.info("Updating to DB : "+str(insert))
                if file_count >= 1:
                    upd_cond = {"phone_number" : int(user_details['phone'])}# condition on whoch the update must happen
                    
                    is_updated = mongo_conn.mongo_update(col_type="input",mode="update",
                        record={"active_call": 1},upd_cond=upd_cond)

                    if is_updated is True:
                        logger.info("Updated the input collection with the update sequence")
                        flow_id = existing_details.get("flow_id")
                        logger.info("Flow Id is {}".format(flow_id))
                    else:
                        logger.info(" Failed to update in the input collection")
                else:
                    logger.info("Adding a new record for : "+str(user_details['phone']))
                    is_inserted = mongo_conn.mongo_update(col_type="input",mode="insert",record=existing_details)

                    if is_inserted is True:
                        logger.info("INSERTED IN REPORT DB ")
                    else:
                        logger.info("Failed to insert in Report DB. Please see logs for errors ")

                status_update = {"call_status":"Updated Call Status Successfully"}
                logger.info("Sending the response: "+str(status_update))
                return response.json(status_update)
            except:
                logger.exception("Exception!! When inserting the new details to DB")
                status_update = {"initiate":"Inserting new details Failed!"}
                return response.json(status_update)    

        @custom_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            logger.info("="*100)
            start_time = time.time()   
            try:
                logger.info("Request recieved : "+str(request.json))
                sender_id = str(await self._extract_sender(request))
                input_channel = self._extract_input_channel(request)
                metadata = self.get_metadata(request)
                asr_text = str(self._extract_message(request))
                mobile = int(self._extract_mobile(request))
                
                logger.info("sender_id: {} | phone number : {} | asr_text :[{}] ".format(
                    sender_id,str(mobile),asr_text),sender_id = sender_id)

                asr_text = asr_text.replace(" o clock"," oclock")
                asr_text = asr_text.replace("200 oclock","2 oclock")
                
                logger.info(f"ASR TEXT -- {asr_text}")


                end_time = time.time()
                response_time = end_time - start_time
                logger.info("After Extracting the request  : " + " " + " || Response time : "+ str(response_time))  

                logger.info(f"ASR TEXT -- {asr_text}")


            except Exception as e:
                logger.exception("Exception in webhook: when extracting parameters" + str(e),sender_id = sender_id)
                client.captureException()
                ## if sender_id id None end the call    
                endMessage = templates["utter_bot_not_understand"]   
                return response.json(endMessage.encode('utf-8').strip().decode("utf-8", "ignore"))
                
            resp_time, last_response_time = get_response_time(start_time)
            logger.info("Time Taken to Extract Parameters  : [{}] ".format(str(resp_time)))  

            res = []

            # ASR text -  handle none
            if asr_text is None or asr_text == '':
                asr_text = templates["set_asr_text"]
            
            asr_text = asr_text.replace("commendation", "accommodation")
                
            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )


            # Logic to fetch the data from the mongo and push it to redis
            try:
                # Connect to the Redis Database and 
                # check if the number is already preset or else load the mongo Database and import to Redis
                # check if the sender-id data mapping is done             
                
                resp_time, last_response_time = get_response_time(last_response_time)

                logger.info("Time Taken to reach record extraction code : [{}] ".format(
                    str(resp_time)))  
                
                redis_data = redis_db_obj.redis_get_dict_value(str(sender_id))              
                
                resp_time, last_response_time = get_response_time(last_response_time)
                logger.info("Time Taken to extract record from redis : [{}] ".format(
                    str(resp_time)))  


                # logger.info("latest data from redis for current request : \n {} \n".format(
                #     str(redis_data)))  

                if redis_data is None:
                    
                    logger.info("Data Not found in redis. Fetching from mongo : ")

                    resp_time, last_response_time = get_response_time(last_response_time)
                    logger.info("Time Taken to extract record from redis : [{}] ".format(
                    str(resp_time)))
                    
                    environment = os.environ["BOT_ENV"]
                   # condition = {
                            # "$or":[{"phone_number":mobile},{
                            #     "alternate_number1":mobile},
                            #     {"alternate_number2":mobile},
                            #     {"alternate_number3":mobile}],
                                
                            # }
                    condition = {"phone_number":mobile}                         
                    

                    logger.info("the type of mobile is {}   ".format(type(mobile)))

                    if environment == "prod":
                        condition["active_call"] = 1
                    else:
                        condition = condition
                    
                    q1 = condition # filter condition to find the document
                    q2 = {"_id": 0}  # 2nd query condition optional, setting id column to 0 , or if only certain fields are req  OPTIONAL
                    logger.info("query is : "+str(q1),sender_id=sender_id)
                    
                    input_collection = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2)
                              
                    resp_time, last_response_time = get_response_time(last_response_time)
                    logger.info("Time Taken to extract record from mongo : [{}] ".format(
                    str(input_collection)))

                    # file_count = mongo_conn.mongo_file_count(col_type="input",q1=condition) 
                    # logger.debug("custom_connector: file count: "+str(file_count))
                    if input_collection is not None:

                        logger.info("Input record obtained from db is  : [{}] ".format(
                        str(input_collection)))
                        input_collection = mongo_conn.mongo_add_required_keys(input_collection)
                        
                        logger.info("Input record obtained afrer modifying is  : [{}] ".format(
                        str(input_collection)))
                        
                        # mapping phone number with dictionary from mongo database
                        logger.info("mapping sender-id with dictionary from mongo database")
                        val = redis_db_obj.redis_set_dict_value_and_expiry(str(sender_id),input_collection,32400)     #SETTING THE DICTIONARY VALUES USING PICKLE
                        
                        resp_time, last_response_time = get_response_time(last_response_time)
                        logger.info("Time Taken to extract record, modify and set to redis : [{}] ".format(
                        str(resp_time)))
          
                        
                    else:
                        ## if phone_number is not in mongo collection
                        logger.info("Number not found for {} and sender id is {}".format(str(mobile),str(sender_id)),)
                        res.append(templates['utter_number_not_found'])
                        return response.json(res[0].encode('utf-8').strip().decode("utf-8", "ignore"))
                        # return response.json(res.strip())
                else:
                    pass
            except Exception as e:
                logger.error("custom_connector: Error!!! when fetching database and setting in redis:"+str(e))
                client.captureException()

          
            resp_time, last_response_time = get_response_time(last_response_time)
            logger.info("Before Hitting Rasa Action Server : [{}] ".format(
            str(resp_time)))

            try:
                if should_use_stream:
                    return response.stream(
                        self.stream_response(
                            on_new_message, asr_text, sender_id, input_channel, metadata
                        ),
                        content_type="text/event-stream",
                    )
                else:
                    collector = CollectingOutputChannel()
                    # noinspection PyBroadException
                    try:
                        await on_new_message(
                            UserMessage(
                                asr_text,
                                collector,
                                sender_id,
                                input_channel=input_channel,
                                metadata=metadata,
                            )
                        )
                    except CancelledError:
                        logger.error(
                            "Message handling timed out for "
                            "user message '{}'.".format(asr_text)
                        )
                    except Exception as e:
                        logger.error(
                            "An exception occured while handling "
                            "user message '{} {}'.".format(asr_text,e)
                        )
                    
                    try:
                        res = list(map(itemgetter('text'), collector.messages))
                        logger.info(f"Response from action server {res}")
                        resp_time, last_response_time = get_response_time(last_response_time)
                        logger.info("Time Taken by Action Server to respond : [{}] ".format(
                        str(resp_time)))
                        # If Core is not able to predict the next action
                        if len(res) < 1 or res is None:
                            res.append(templates['utter_fallback'])
                        else:
                            pass

                    except Exception as e:
                        logging.exception("Exception: Collector messages not received: "+str(e))
                        res.append(templates['utter_fallback'])

                    logger.info("Reply from action server : "+str(res))

                    if "fallback_word" in res[0]:
                        res[0] = res[0].replace("fallback_word", "")
                    
                    res[0] = res[0].replace(",","~")
                        
                    # Code for Saving the Conversation Transcription
                    try:
                        
                        current_date = datetime.now().strftime("%Y-%m-%d")
                        log_dict = {"asr_transcript":asr_text,"bot_response":res[0].replace('EOC','').replace('TTA','')}
                        logger.info("========================================================")    
                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info(" Saving the Conversation  before reading redis data: " + " " + " || Response time : "+ str(response_time))  
                        logger.info("========================================================")    

                        update_dict = redis_db_obj.redis_get_dict_value(str(sender_id))

                        resp_time, last_response_time = get_response_time(last_response_time)
                        logger.info("Time Taken to fetch data from redis to store conversation logs : [{}] ".format(
                        str(resp_time)))
                        
                        if update_dict is not None:

                            if "conversation_log" in update_dict.keys():
                                logger.info(f"updated dict : {update_dict}")
                                log_dict["confidence"] = update_dict["tracker_log"][-1]["confidence"]
                                log_dict["intent"] = update_dict["tracker_log"][-1]["intent"]
                                update_dict['conversation_log'].append(log_dict)
                                redis_db_obj.redis_set_dict_value_and_expiry(str(sender_id),update_dict,32400)
                                
                                resp_time, last_response_time = get_response_time(last_response_time)
                                logger.info("Time Taken to store conversation logs : [{}] ".format(
                                str(resp_time)))
                        
                            else:
                                logger.info("conversation_log key is not present")
                                pass

                        else:
                            logger.info("None type dict for sender_id"+str(sender_id)+"when writing converstion logs")
                            pass
                
                        
                    except Exception as e:
                        client.captureException()
                        logging.exception("Exception When Writing Conversation Logs in the Custom Connector: "+str(e))
                    
                    
                    resp_time, last_response_time = get_response_time(start_time)
                    logger.info("Time Taken to Send back response:{} | Overall response time: [{}] ".format(str(res[0]),
                    str(resp_time)))
                    
                    logger.info("exiting webhook method ")
                    utterance = res[0].encode('utf-8').strip().decode("utf-8", "ignore")
                    
                    language = update_dict.get("language")
                    #utterance = post_process_date_time(utterance,lang=language)
                    
                    try:
                        user = redis_db_obj.redis_get_dict_value(str(sender_id))
                        flow_id = user.get("flow_id")
                        logger.info(f"flow_id from user {flow_id}")
                        response_text= modify_response(sender_id,utterance.encode('utf-8').strip().decode("utf-8", "ignore"),flow_id=flow_id)
                        response_text = response.json(response_text) 
                    except Exception as e:
                        user = redis_db_obj.redis_get_dict_value(str(sender_id))
                        flow_id = user.get("flow_id")
                        logger.info(f"flow_id from user {flow_id}")
                        logger.exception("Exception occured in the webhook endpoint : "+str(e))
                        response_text = modify_response(sender_id, templates["utter_fallback"],flow_id=flow_id)
                        response_text  = response.json(response_text)

                    logger.info("***Response_Text***: "+str(response_text))
                    return response_text

           
            except Exception as e:
                logger.exception('Exception when sending response:'+str(e))
                client.captureException()

        return custom_webhook



