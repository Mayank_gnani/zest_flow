import sys
sys.path.append('../')

from utils.date_valid import validate_date, haptik_date_validation
from utils.load_meta import BOTCODE, THRESHOLD
from db.tracker_store_connection import load_config, RedisDB
from logger_conf.logger import get_logger, CustomAdapter
from rasa_sdk.forms import FormAction
from raven import Client


import dateutil.relativedelta as dr
from pytz import timezone
from actions.get_config import redis_db_obj, templates, city_list
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, Restarted, \
    EventType
from rasa_sdk import Action, Tracker
from utils.search_algorithm import search
import json 
from typing import Any, Text, Dict, List, Union, Optional

logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})

# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
# redis connection
redis_db_obj = RedisDB()

# with open("../configs/product_description_audio.json", "r", encoding="utf-8") as temp_desc:
#     description = json.load(temp_desc)


def get_stage_codes(stage_name, stage_code, flow_type):
    stage_updates = {}

    stage_updates["stage"] = stage_name
    stage_updates[BOTCODE] = stage_code

    return stage_updates


def template_resolver(language, key):
    lang_dict = {
        'english': templates
    }

    if language in lang_dict:
        return lang_dict[language][key]


##----------------------------------------------------------------------------------------------------------##
class ActionRepeat(Action):
    """
    Repeat the bot response again if user asked to repeat, also
    repeat the bot response if user doesn't say anything
    If asked continously asked to repeat for 2 times 3rd time
    end the call
    return: dispatch message for who called if silent after initial message
    return: dispatcher message for previous response
    """

    def name(self) -> Text:
        return "action_repeat"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:
            logger.info("Executing action : {}".format(action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)
            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info("inside repeat action: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            # tracker details
            text = tracker.latest_message['text']
            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name) 
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            name = cust_info['full_name']
            language = cust_info['language'].lower()
            logger.info("language is :  "+str(language),sender_id=sender_id)
            bot_msg = ""

            for event in reversed(tracker.events):
                if event.get("event") == "bot":
                    bot_not_answered = False
                    bot_msg = event.get("text")

                    break

                else:
                    bot_not_answered = True

            repeat = 0
            for event in tracker.events:
                if event.get("event") == "bot":
                    # logger.info("action_repeat : sender id:" + str(sender_id) + " Bot has answered! ", sender_id=sender_id)
                    key = 'initial_message'
                    message = template_resolver(language, key)

                    if event.get("text") == message.replace("name_", name):
                        logger.info("setting true bot not answered ", sender_id=sender_id)
                        bot_not_answered = True
                    else:
                        logger.info("setting true bot not answered ", sender_id=sender_id)
                        bot_not_answered = False

                # logger.info("INSIDE REPEAT ACTION *** GET USER EVENT : "+ str(event.get("event"))) #+ "INTENT IS "+ str(event.get("parse_data")["intent"]["name"]))
                if event.get("event") == "user" and \
                        event.get("parse_data")["intent"]["name"] == "intent_repeat":
                    repeat += 1
                    logger.info("ActionRepeat: increment" + str(repeat), sender_id=sender_id)

                    ## update repeat count
                    final_dict = {
                        "repeat_count": repeat,
                        "action": "action_repeat"
                    }
                    is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                elif event.get("event") == "user" and event.get("parse_data")["intent"]["name"] != "intent_repeat":
                    repeat = 0

            logger.info("increment after loop--> " + str(repeat), sender_id=sender_id)
            if repeat > 2:
                logger.info("ActionRepeat: Repeat Action", sender_id=sender_id)
                stage = "repeat:failure"
                TVSCS = "DSCN"

                stage_updates = get_stage_codes(stage, TVSCS, cust_info["flow_type"])
                final_dict = {
                    "repeat_failure": "yes",
                    "action": "action_repeat"
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                key = 'utter_bot_not_understand'
                message = template_resolver(language, key)
                dispatcher.utter_message(message)
                return []
                # Call is Ended EOC
            else:
                logger.info("BOT NOT ANSWERED VALUE IN ELSE PART OF REPEAT ACTION " + str(bot_not_answered),
                            sender_id=sender_id)
                logger.debug("ActionRepeat: Repeat Action", sender_id=sender_id)
                if bot_not_answered:
                    stage = "repeat:initial_message"
                    TVSCS = "DSCN"

                    stage_updates = get_stage_codes(stage, TVSCS, cust_info["flow_type"])
                    final_dict = {
                        "call_comp_flag": "no",
                        "action": "action_repeat"
                    }
                    is_updated = redis_db_obj.update_stage(tracker, final_dict)

                    logger.info("bot did not utter anything", sender_id=sender_id)
                    key = 'initial_message'
                    message = template_resolver(language, key)
                    dispatcher.utter_message(message)
                    return [UserUtteranceReverted()]
                else:
                    logger.info("bot_not_answered is false", sender_id=sender_id)
                    logger.info("Bot Message: " + str(bot_msg), sender_id=sender_id)
                    dispatcher.utter_message(bot_msg)
                    return [UserUtteranceReverted()]

        except Exception as e:
            logger.exception(" Exception in action repeat --> " + str(e), sender_id=sender_id)
            client.captureException()
            return []


##----------------------------------------------------------------------------------------------------------## 

class ActionIntial(Action):
    
    def name(self) -> Text:
        return "action_intial"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "intial"
                CODE = "DSCN"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'initial_message'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []





class ActionTellEmi(Action):

    def name(self) -> Text:
        return "action_EMI"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "EMI"
                CODE = "INTL"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_EMI'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []


class ActionThanks(Action):
    
    def name(self) -> Text:
        return "action_thanks"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "thanks"
                CODE = "CMPLTD"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_thanks'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []




class ActionLater(Action):
    
    def name(self) -> Text:
        return "action_later"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "later"
                CODE = "INTIL"
                date=tracker.get_slot("date")
                time=tracker.get_slot("time")

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_later'
                message = template_resolver(language, key)
                logger.info(date)
                logger.info(time)
                dispatcher.utter_message(message.format(date=date))
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []



class ActionPayment(Action):
    
    def name(self) -> Text:
        return "action_payment"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "payment"
                CODE = "PRTL"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_payment'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []



class ActionCovid(Action):
    
    def name(self) -> Text:
        return "action_covid"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "covid"
                CODE = "CMPLTD"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_covid'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []


class ActionSorry(Action):
    
    def name(self) -> Text:
        return "action_sorry"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "sorry"
                CODE = "CMPLTD"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_sorry'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []


class ActionPaylater(Action):
    
    def name(self) -> Text:
        return "action_paylater"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "paylater"
                CODE = "PRTL"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_paylater'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []


class ActionConnect(Action):
    
    def name(self) -> Text:
        return "action_connect"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:

            logger.info(f"Executing action : {action_name}", sender_id=sender_id)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info(f"{action_name} : customer data is not present", sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            
            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(f"text - {text}\nintent - {intent}\nconfidence - {confidence}")

            if round(float(confidence), 2) > THRESHOLD:
                stage = "connect"
                CODE = "PRTL"

                final_dict = {
                    "stage": stage,
                    "call_comp_flag": "no",
                    BOTCODE: CODE,
                    "action": action_name,
                }

                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                
                key = 'utter_connect'
                message = template_resolver(language, key)
                
                dispatcher.utter_message(message)
                return []
            else:
                # wrong flow
                return [FollowupAction("action_fallback")]
        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []















