#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: get_config.py
# Description: Script to get config objects
# Author: Avinash
# Date: 11-01-2021

from db.tracker_store_connection import load_config, MongoDB, RedisDB
from raven import Client
import json

from logger_conf.logger import get_logger
from logger_conf.logger import CustomAdapter
from datetime import date

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
client_flag = gen_config["raven"]["flag"]
# redis connection
redis_db_obj = RedisDB()
mongo_conn = MongoDB()

templates = {}
with open("../configs/templates_text.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)

f =  open("../data/cities.txt")
city_ = f.readlines()
city_list = [city.strip("\n").lower() for city in city_]

hindi_dict_mapping = {
    "January": "जनुअरी",
    "February": "फेब्रुअरी",
    "March": "मार्च", 
    "April": "अप्रैल", 
    "May": "मई", 
    "June": "जून", 
    "July": "जुलाई", 
    "August": "ऑगस्ट", 
    "September": "सितम्बर", 
    "October": "अक्टूबर", 
    "November": "नवंबर", 
    "December": "दिसंबर" 
}

def capture_exception():
    logger.info("Client flag is :"+str(client_flag))
    logger.info(client_flag)
    if client_flag:
        return client.captureException()
    else:
        return None

def log_redis_update(update_flag, action_name, s_id):
    if update_flag is True:
        logger.info("Updated stage in redis {}".format(action_name),sender_id=s_id)
    else:
        logger.info("failed to update in redis {}".format(action_name),sender_id=s_id)

def reform_date(string_date):
    
    if "/" in string_date:
        string_date = string_date.split("/")
        new_date = date(day=int(string_date[0]), month=int(string_date[1]), year=int(string_date[2])).strftime('%d %B %Y')
        new_date = new_date.lower()
        for eng, hin in hindi_dict_mapping.items():
            eng = eng.lower()
            new_date = new_date.replace(eng,hin)
    else:
        new_date = string_date
    #logger.info("Reformed Date is : "+str(new_date))
    return new_date

def get_replaced_message(template_message, sender_id, cust_info):
    """function to get value in string form of a key from redis

    Args:
        template_messgae (string): message from templates.json
        sender_id (string): sender id of the conversation
        cust_info (dictionary): cust info from dictionary

    Returns:
        string: actual template message to be uttered
    """
    try:
       
        variable_replacement_map = {}

        # replace the variable name in the templates with variable value extracted from db
        logger.debug(str(variable_replacement_map))
        for var_name, actual_variable in variable_replacement_map.items():
            template_message = template_message.replace(var_name,str(actual_variable))

        template_message = template_message
    
    except Exception as e:
        logger.info("Exception in dynamically changing the template: "+ str(e),sender_id=sender_id)
        capture_exception()

    return template_message
