#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: action_fallback.py
# Description: Action to handle fallback scenarios
# Author: Avinash
# Date: 11-01-2021

import sys
sys.path.append('../')

from typing import Any, Text, Dict, List
import json
from rasa_sdk import Action, Tracker
from rasa_sdk.events import  UserUtteranceReverted, ActionReverted, Restarted
from rasa_sdk.executor import CollectingDispatcher


from pytz import timezone
import dateutil.relativedelta as dr

from actions.get_config import redis_db_obj, capture_exception, log_redis_update, get_replaced_message, templates
from logger_conf.logger import get_logger
from logger_conf.logger import CustomAdapter

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

with open("../configs/templates_text.json", "r", encoding="utf-8") as temp:
    fallback_key = json.load(temp)

class ActionFallback(Action):

    def name(self) -> Text:
        return "action_fallback"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # DB updated for stage
        try:
            sender_id = tracker.current_state()["sender_id"]
            action_name = self.name()
            tracker_list = redis_db_obj.tracker_details(tracker, action_name)
            sender_id,cust_info = redis_db_obj.get_details(tracker)

            if cust_info is None:
                logger.info("action_fallback: Phone number not found hence ending call ",sender_id=sender_id)
                dispatcher.utter_message(text="EOC")
                return [Restarted()]
          
            version = cust_info.get("flow_id")
            language = cust_info['language'].lower()


            intent = tracker.latest_message.get("intent", {}).get("name")
            user_txt = tracker.latest_message['text'] 

            logger.info("Action Fallback : intent identified as: " +str(intent),sender_id=sender_id)

            #if it comes to fallback
            final_dict = cust_info
            prev_action = cust_info["action"]
            
            conv_dict = {
                "current_text":user_txt,
                "intent":intent,
                "previous_action": prev_action
            }
            final_dict["call_analysis"]=1

            logger.info("conv log in fallback "+str(conv_dict),sender_id=sender_id)
            final_dict["fallback_conv"].append(conv_dict)

            is_updated = redis_db_obj.update_stage(tracker,final_dict)
            log_redis_update(is_updated,action_name, sender_id)

            templates_temp = {}
          
            for key,message in templates.items():
                
                message = get_replaced_message(message,sender_id,cust_info) 

                templates_temp[key] = message
            
            # Checking for 2 fallback, if bot didn't understand final message is uttered.
            bot_msg  = "" 
            count = 0
            fallback_key = "fallback_word"

            # Fallback break logic
            for event in reversed(tracker.events):
                if event.get("event") == "bot":
                    if fallback_key in event.get("text"):
                        count += 1
                        if count == 2:
                            logger.info("Bot Failed to understand. Ending the call ",sender_id=sender_id)
                            stage = "fallback:3times"
                            BOTCODE = "DSCN"

                            stage_updates = {} #get_stage_codes(stage, BOTCODE, cust_info["flow_type"])

                            final_dict = {
                                "action":"action_fallback",
                                "call_comp_flag": "no",
                                "fallback_failure": "yes"
                            }
                            is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                            log_redis_update(is_updated,action_name, sender_id)

                            dispatcher.utter_message(templates["utter_bot_not_understand"])
                            return [Restarted()]
                    else:
                        count = 0
                        break

            for event in reversed(tracker.events):
                if event.get("event") == "bot":

                    if fallback_key in event.get("text"):
                        count += 1
                        
                        logger.info("Fallback not greater than 2")
                        #count += 1
                        bot_msg = event.get("text")
                        bot_msg, short_key_present = get_bot_message(event, bot_msg, templates_temp)
                        logger.info("Bot message after function call : "+str(bot_msg))
                        break
                    else:
                        logger.info("Fallback message not found")
                        bot_msg = event.get("text")
                        bot_msg,short_key_present = get_bot_message(event, bot_msg, templates_temp)
                        logger.info("Bot message after function call : "+str(bot_msg))
                        #if bot_msg is not None:
                        count += 1
                        break
            
            
            
            logger.info("Fallback count is : "+str(count))
            if bot_msg == "":
                #final_dict = {"stage":"initial_message","bot_not_answered":False}
                stage = "fallback:initial_message"
                BOTCODE = "DSCN"

                stage_updates = {} #get_stage_codes(stage, BOTCODE, cust_info["flow_type"])

                final_dict = {

                    "call_comp_flag": "no",
                    "action": "action_fallback"
                }
                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                log_redis_update(is_updated,action_name, sender_id)
                
                # msg = templates["initial_message"]+fallback_key
                msg = templates["initial_message"]
                dispatcher.utter_message(msg)
            else:
                #update fallback count
                
                final_dict = {
                    "fallback_count": count,
                    "action":"action_fallback",
                    #"cntr_var":"action_fallback"
                }
                is_updated = redis_db_obj.update_stage(tracker,final_dict)
                log_redis_update(is_updated,action_name, sender_id)
                if templates["utter_fallback"] not in bot_msg and not short_key_present:
                    bot_msg =  templates["utter_fallback"] + " ~ " + bot_msg

                dispatcher.utter_message((bot_msg+fallback_key).replace(' fallback_word fallback_word',' fallback_word')) 
            
            logger.info("Exiting Fallback function",sender_id=sender_id)
            return [UserUtteranceReverted()]
        
        except Exception as e:
            logger.exception("action_fallback : fallback failed for "+str(sender_id)+ "Exception is : "+str(e),sender_id=sender_id)
            capture_exception()

            return [UserUtteranceReverted(),ActionReverted()]


def get_bot_message(event, bot_msg, templates_temp):
    
    bot_msg = bot_msg.replace("fallback_word","")
    
    #logger.info("bot msg is :"+str(bot_msg))
    short_key_present = False
    for template_key,template_message in templates_temp.items():
        #logger.info("bot msg is :"+str(bot_msg))
        
        # logger.info("Current template key is {} and current template_message is : {}".format(
        #     template_key,template_message))
        
        if bot_msg == template_message:
            logger.info("Template key is : "+str(template_key))
            if template_key.endswith("_short"):
                logger.info("Key endswith short : "+str(template_key))
                short_key = template_key.replace("_short","_short_2") 
                short_key_present = True
            else:
                logger.info("Key does not endswith short : "+str(template_key))
                short_key = template_key + "_short"
                short_key_present = True
        
        
            if short_key in templates_temp:
                bot_msg = templates_temp[short_key]
                break
            elif template_key + "_short" in templates_temp:
                bot_msg = templates_temp[short_key]
                break
            else:
                if "who" in template_key or  "faq" in template_key:
                    continue
                else:
                    bot_msg = templates_temp[template_key]
                    break
   
    logger.info("Bot message returned is :"+str(bot_msg))
    return bot_msg, short_key_present
