#!/usr/bin/env python3
# -- coding: utf-8 --

# File name: logger.py
# Description: Script to handle the logging function
# Author: Avinash
# Date: 15-10-2020
import sys
sys.path.append('./')

import logging.handlers
import logging
from pytz import timezone, utc
from datetime import datetime
import sys


FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s - %(process)d — %(funcName)s:%(lineno)d — %(message)s")

class CustomAdapter(logging.LoggerAdapter):
    """
    This example adapter expects the passed in dict-like object to have a
    'connid' key, whose value in brackets is prepended to the log message.
    """
    def process(self, msg, kwargs):
        sender_id = kwargs.pop('sender_id', self.extra['sender_id'])
        return '[%s] %s' % (sender_id, msg), kwargs


def custom_time(*args):
    """
    get the customised time
    :return:
    """
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Asia/Kolkata")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

def get_logger(name):
    """
    get the customised logger for the given python file
    :param name:
    :return:
    """
    logger = logging.getLogger(name)
    if not logger.handlers:
        logger.propagate = 0
        logger.setLevel("INFO")
        logging.Formatter.converter = custom_time
        handler = logging.StreamHandler(sys.stdout)
        #logging_file = logging.FileHandler('log_file.log')
        handler.setFormatter(FORMATTER)
        file_handler = logging.FileHandler('log_file.log')
        file_handler.setFormatter(FORMATTER)
        
        logger.addHandler(file_handler)
        logger.addHandler(handler)
        
    return logger