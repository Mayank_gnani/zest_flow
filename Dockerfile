FROM gnanivb.azurecr.io/rasa:1.10.16.v1.1
WORKDIR /nlp_server
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
RUN mkdir -p /word_vector/
# RUN mv te_vectors_wiki_lg /word_vector/
# RUN ["/bin/bash", "train.sh"]
RUN python -m spacy download en_core_web_md
RUN python -m spacy link en_core_web_md en
EXPOSE 5002
ENTRYPOINT ["/bin/bash", "docker-entrypoint.sh"]
