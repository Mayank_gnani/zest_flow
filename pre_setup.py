import os
import json
import yaml

def load_meta():
    meta = ""
    try:
    
        meta_data = "meta.yml" 
        with open('./configs/'+meta_data, 'r') as f:
            meta = yaml.load(f, Loader=yaml.FullLoader) 
    
    except Exception as e:
        print(f"Error loading the meta data from the meta.yml "+str(e))
    return meta  


params = load_meta()['parameters']
model_path = params["model_path"]
print(model_path)
model_name = params["model_name"]
print(model_name)
download_s3 = "aws s3 cp model_path .".replace("model_path",model_path)

os.system("mkdir models")
print("downloading from s3")
print(download_s3)
os.system(download_s3)
print("extracting from s3")
os.system("tar xvf model_name".replace("model_name",model_name))
os.system("mv model_name core/ nlu/ fingerprint.json models/".replace("model_name",model_name))
